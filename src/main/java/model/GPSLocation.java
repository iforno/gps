package model;

public class GPSLocation {
	private double latitude;
	private double longtitude;
	
	public GPSLocation() {
		// TODO Auto-generated constructor stub
	}

	public GPSLocation(double latitude, double longtitude) {
		super();
		this.latitude = latitude;
		this.longtitude = longtitude;
	}
	
	public double getLatitude() {
		return latitude;
	}
	
	public double getLongtitude() {
		return longtitude;
	}
	
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	public void setLongtitude(double longtitude) {
		this.longtitude = longtitude;
	}

}
