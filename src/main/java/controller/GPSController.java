package controller;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import model.GPSLocation;

@Controller
@RequestMapping(path = "/location")
public class GPSController {

	
	@GetMapping(path = "/get")
	public @ResponseBody ResponseEntity<GPSLocation> get() {
		
		return new ResponseEntity<GPSLocation>(new GPSLocation(1.9,2.3),HttpStatus.OK);
	}
	@GetMapping(path = "/all")
	public @ResponseBody Iterable<GPSLocation> all(){
		GPSLocation g1 = new GPSLocation(1.0,3.0);
		GPSLocation g2 = new GPSLocation(2.0,5.0);
		ArrayList<GPSLocation> list = new ArrayList<GPSLocation>();
		list.add(g1);
		list.add(g2);
		return list;
	}
}
