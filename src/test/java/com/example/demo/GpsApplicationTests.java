package com.example.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import controller.GpsApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GpsApplication.class)
public class GpsApplicationTests {

	@Test
	public void contextLoads() {
	}

}
